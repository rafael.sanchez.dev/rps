package com.rsr.rps.model;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * @author Rafa Sanchez
 */
@RunWith(MockitoJUnitRunner.class)
public class ScissorsTest {

    @InjectMocks
    Scissors scissors;

    @Test
    public void playAgainstReturnTieWhenScissorAgainstScissor() {
        Shape secondScissor = new Scissors();

        PlayResult result = scissors.playAgainst(secondScissor);

        assertThat(result, is(PlayResult.DRAW));
    }

    @Test
    public void playAgainstReturnLooseWhenScissorAgainstRock() {
        Shape rock = new Rock();

        PlayResult result = scissors.playAgainst(rock);

        assertThat(result, is(PlayResult.LOOSE));
    }

    @Test
    public void playAgainstReturnWinWhenScissorAgainstPaper() {
        Shape paper = new Paper();

        PlayResult result = scissors.playAgainst(paper);

        assertThat(result, is(PlayResult.WIN));
    }

}
