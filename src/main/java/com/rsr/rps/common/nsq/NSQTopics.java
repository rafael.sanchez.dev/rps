package com.rsr.rps.common.nsq;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Rafa Sanchez
 */
@Getter
@Setter
class NSQTopics {
    private List<String> topics;
}