#!/bin/bash
echo "Starting NSQ containers from scratch..."
docker kill $(docker ps -a -q)
docker rm $(docker ps -q -f status=exited)
pwd
sleep 3
docker-compose -f docker-compose.yml up -d
sleep 3
docker ps
