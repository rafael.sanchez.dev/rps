package com.rsr.rps.model;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * @author Rafa Sanchez
 */
@RunWith(MockitoJUnitRunner.class)
public class RockTest {

    @InjectMocks
    Rock rock;

    @Test
    public void playAgainstReturnTieWhenRockAgainstRock() {
        Rock secondRock = new Rock();

        PlayResult result = rock.playAgainst(secondRock);

        assertThat(result, is(PlayResult.DRAW));
    }

    @Test
    public void playAgainstReturnWinWhenRockAgainstScissor() {
        Scissors scissors = new Scissors();

        PlayResult result = rock.playAgainst(scissors);

        assertThat(result, is(PlayResult.WIN));
    }

    @Test
    public void playAgainstReturnLooseWhenRockAgainstPaper() {
        Paper paper = new Paper();

        PlayResult result = rock.playAgainst(paper);

        assertThat(result, is(PlayResult.LOOSE));
    }

}
