package com.rsr.rps.client;

import static com.google.common.net.HttpHeaders.USER_AGENT;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Component;

import com.rsr.rps.common.config.RpsConfig;
import com.rsr.rps.common.exceptions.ExternalSystemException;
import com.rsr.rps.service.RpsStatisticsParser;
import com.rsr.rps.service.RpsStatisticsResponse;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RpsStatisticsClient {
    private static final String PROTOCOL = "http://";

    private final RpsStatisticsParser parser;
    private final RpsConfig config;

    public RpsStatisticsClient(RpsStatisticsParser parser, RpsConfig config) {
        this.parser = parser;
        this.config = config;
    }

    public RpsStatisticsResponse get() {
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(buildGetRequest());
            return parser.parseStatistics(response);
        } catch (IOException e) {
            log.error("Error when getting the statistics - {}" + e.getMessage());
            throw new ExternalSystemException(e.getMessage());
        }
    }

    private HttpGet buildGetRequest() {
        HttpGet request = new HttpGet(buildStatisticsUri());
        request.addHeader("User-Agent", USER_AGENT);
        return request;
    }

    private String buildStatisticsUri() {
        return PROTOCOL + config.getStatisticsHttpHost() + ":" + config.getStatisticsHttpPort() + "/" + config.getStatisticsEndpoint();

    }
}
