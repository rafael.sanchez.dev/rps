package com.rsr.rps.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rsr.rps.client.RpsStatisticsClient;
import com.rsr.rps.client.StatisticsProducer;

/**
 * @author Rafa Sanchez
 */
@Service
public class RpsStatisticsService {

    private final StatisticsProducer statisticsProducer;
    private final RpsStatisticsClient rpsStatisticsClient;

    @Autowired
    public RpsStatisticsService(StatisticsProducer statisticsProducer, RpsStatisticsClient rpsStatisticsClient) {
        this.statisticsProducer = statisticsProducer;
        this.rpsStatisticsClient = rpsStatisticsClient;
    }

    public void addPlayResultToStatistic(String playResult) {
        statisticsProducer.postMessage(playResult);
    }

    public RpsStatisticsResponse getStatistics() {
        return rpsStatisticsClient.get();
    }
}
