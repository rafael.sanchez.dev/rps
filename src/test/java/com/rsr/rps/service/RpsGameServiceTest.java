package com.rsr.rps.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.rsr.rps.model.PlayResult;

/**
 * @author Rafa Sanchez
 */
@RunWith(MockitoJUnitRunner.class)
public class RpsGameServiceTest {

    private static final String ROCK = "ROCK";
    private static final String SCISSORS = "SCISSORS";

    @Mock
    private RpsStatisticsService rpsStatisticsService;

    @InjectMocks
    RpsGameService rpsGameService;

    @Test
    public void playRoundReturnDrawWhenPlayerShapesAreEquals() {

        RpsGameResponse response = rpsGameService.playRound(ROCK, ROCK);

        assertThat(response.getMessage(), is(PlayResult.DRAW.getMessage()));
        verify(rpsStatisticsService).addPlayResultToStatistic(PlayResult.DRAW.getMessage());
    }

    @Test
    public void playRoundReturnPlayer1WinsWhenPlayer1ShapeIsWinner() {

        RpsGameResponse response = rpsGameService.playRound(ROCK, SCISSORS);

        assertThat(response.getMessage(), is(PlayResult.WIN.getMessage()));
        verify(rpsStatisticsService).addPlayResultToStatistic(PlayResult.WIN.getMessage());
    }

    @Test
    public void playRoundReturnPlayer2WinsWhenPlayer2ShapeIsWinner() {

        RpsGameResponse response = rpsGameService.playRound(SCISSORS, ROCK);

        assertThat(response.getMessage(), is(PlayResult.LOOSE.getMessage()));
        verify(rpsStatisticsService).addPlayResultToStatistic(PlayResult.LOOSE.getMessage());
    }
}
