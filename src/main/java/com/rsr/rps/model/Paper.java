package com.rsr.rps.model;

import lombok.Builder;

/**
 * @author Rafa Sanchez
 */
@Builder
public class Paper implements Shape {

    @Override
    public PlayResult playAgainst(Shape shape) {
        if(shape instanceof Paper) {
            return PlayResult.DRAW;
        } else if(shape instanceof Scissors) {
            return PlayResult.LOOSE;
        }
        return PlayResult.WIN;
    }


}
