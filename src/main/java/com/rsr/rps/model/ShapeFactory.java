package com.rsr.rps.model;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * @author Rafa Sanchez
 */
public class ShapeFactory {
    private static final Map<String, Supplier<Shape>> map = new HashMap<>();
    static {
        map.put("PAPER", Paper::new);
        map.put("ROCK", Rock::new);
        map.put("SCISSORS", Scissors::new);
    }

    private ShapeFactory() { }

    public static Shape getShape(String shapeType){
        if(shapeType != null) {
            Supplier<Shape> shape = map.get(shapeType.toUpperCase());
            if (shape != null) {
                return shape.get();
            }
        }
        throw new IllegalArgumentException("Invalid player's shape " + shapeType);
    }
}