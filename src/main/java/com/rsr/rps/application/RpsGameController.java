package com.rsr.rps.application;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rsr.rps.service.RpsGameResponse;
import com.rsr.rps.service.RpsGameService;

/**
 * @author Rafa Sanchez
 */
@RestController
public class RpsGameController {

    private final RpsGameService rpsGameService;
    private final ResponseContentBuilder<RpsGameResponse> responseContentBuilder;

    @Autowired
    public RpsGameController(RpsGameService rpsGameService, ResponseContentBuilder<RpsGameResponse> responseContentBuilder) {
        this.rpsGameService = rpsGameService;
        this.responseContentBuilder = responseContentBuilder;
    }

    @CrossOrigin
    @GetMapping(value = "/playround")
    public ResponseEntity<RpsGameResponse> playRound(@QueryParam("player1") String player1, @QueryParam("player2") String player2) {
        return responseContentBuilder.buildResponse(HttpStatus.OK, rpsGameService.playRound(player1, player2));
    }

    @ExceptionHandler
    void handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }
}
