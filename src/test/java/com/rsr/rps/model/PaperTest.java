package com.rsr.rps.model;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * @author Rafa Sanchez
 */
@RunWith(MockitoJUnitRunner.class)
public class PaperTest {

    @InjectMocks
    Paper paper;

    @Test
    public void playAgainstReturnTieWhenPaperAgainstPaper() {
        Paper secondPaper = new Paper();

        PlayResult result = paper.playAgainst(secondPaper);

        assertThat(result, is(PlayResult.DRAW));
    }

    @Test
    public void playAgainstReturnLooseWhenPaperAgainstScissor() {
        Scissors scissors = new Scissors();

        PlayResult result = paper.playAgainst(scissors);

        assertThat(result, is(PlayResult.LOOSE));
    }

    @Test
    public void playAgainstReturnWinWhenPaperAgainstRock() {
        Rock rock = new Rock();

        PlayResult result = paper.playAgainst(rock);

        assertThat(result, is(PlayResult.WIN));
    }

}
