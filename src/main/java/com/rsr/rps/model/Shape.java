package com.rsr.rps.model;

/**
 * @author Rafa Sanchez
 */
public interface Shape {

    PlayResult playAgainst(Shape shape);
}
