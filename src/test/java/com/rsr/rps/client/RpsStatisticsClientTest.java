package com.rsr.rps.client;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.rsr.rps.common.config.RpsConfig;
import com.rsr.rps.common.exceptions.ExternalSystemException;
import com.rsr.rps.service.RpsStatisticsParser;

/**
 * @author Rafa Sanchez
 */
@RunWith(MockitoJUnitRunner.class)
public class RpsStatisticsClientTest {

    @Mock
    HttpClient client;

    @Mock
    private RpsConfig config;

    @Mock
    private HttpResponse mockHttpResponse;

    @Mock
    private RpsStatisticsParser parser;

    @InjectMocks
    RpsStatisticsClient rpsStatisticsClient;

    @Test
    public void getReturnSuccess() {
        when(config.getStatisticsHttpHost()).thenReturn("localhost");
        when(config.getStatisticsHttpPort()).thenReturn(9091);
        when(config.getStatisticsEndpoint()).thenReturn("rpsstatistics");

        rpsStatisticsClient.get();

        verify(parser).parseStatistics(any());
    }

    @Test(expected = ExternalSystemException.class)
    public void getThrowsException() {

        rpsStatisticsClient.get();

    }


}