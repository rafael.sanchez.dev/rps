package com.rsr.rps.application;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author Rafa Sanchez
 */
@Component
public class ResponseContentBuilder<T> {

    public ResponseEntity<T> buildResponse(HttpStatus status, T response) {
        return ResponseEntity.status(status).body(response);
    }
}
