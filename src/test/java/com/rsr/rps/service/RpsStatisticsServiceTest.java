package com.rsr.rps.service;

import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.rsr.rps.client.StatisticsProducer;
import com.rsr.rps.model.PlayResult;

/**
 * @author Rafa Sanchez
 */
@RunWith(MockitoJUnitRunner.class)
public class RpsStatisticsServiceTest {

    @Mock
    private StatisticsProducer producer;

    @InjectMocks
    private RpsStatisticsService rpsStatisticsService;

    @Test
    public void addPlayResultToStatisticsReturnsSuccessWhenEverythingGoesFine() {

        rpsStatisticsService.addPlayResultToStatistic(PlayResult.DRAW.getMessage());

        verify(producer).postMessage(PlayResult.DRAW.getMessage());
    }
}
