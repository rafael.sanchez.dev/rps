package com.rsr.rps.service;

import lombok.Builder;
import lombok.Getter;

/**
 * @author Rafa Sanchez
 */
@Getter
@Builder
public class RpsGameResponse {
    private String message;
}
