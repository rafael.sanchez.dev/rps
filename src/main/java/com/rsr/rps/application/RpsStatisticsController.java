package com.rsr.rps.application;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rsr.rps.common.exceptions.ExternalSystemException;
import com.rsr.rps.service.RpsStatisticsResponse;
import com.rsr.rps.service.RpsStatisticsService;

/**
 * @author Rafa Sanchez
 */
@RestController
public class RpsStatisticsController {

    private final RpsStatisticsService rpsStatisticsService;
    private final ResponseContentBuilder<RpsStatisticsResponse> responseContentBuilder;

    @Autowired
    public RpsStatisticsController(RpsStatisticsService rpsStatisticsService,
            ResponseContentBuilder<RpsStatisticsResponse> responseContentBuilder) {
        this.rpsStatisticsService = rpsStatisticsService;
        this.responseContentBuilder = responseContentBuilder;
    }

    @CrossOrigin
    @GetMapping(value = "/statistics")
    public ResponseEntity<RpsStatisticsResponse> getRpsStatistics() {
        return responseContentBuilder.buildResponse(HttpStatus.OK, rpsStatisticsService.getStatistics());
    }

    @ExceptionHandler
    void handleExternalSystemException(ExternalSystemException e, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.SERVICE_UNAVAILABLE.value());
    }
}
