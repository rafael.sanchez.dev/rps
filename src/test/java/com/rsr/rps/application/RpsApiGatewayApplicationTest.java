package com.rsr.rps.application;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import com.rsr.rps.service.RpsGameResponse;
import com.rsr.rps.service.RpsGameService;
import com.rsr.rps.service.RpsStatisticsResponse;
import com.rsr.rps.service.RpsStatisticsService;

/**
 * @author Rafa Sanchez
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RpsApiGatewayApplicationTest {

    private static final String GET_STATISTICS_REQUEST = "/statistics";
    private static final String PLAY_ROUND_REQUEST_PATH_PARAMS = "/playround";
    private static final String MESSAGE = "message";
    private static final String PLAYER1 = "player1";
    private static final String PLAYER2 = "player2";
    private static final String STATS_JSON_RESPONSE = "{'totalRoundPlayed':4, 'totalDraws':1, 'totalPlayer1Wins':1, 'totalPlayer2Wins':2}";

    @MockBean
    private RpsGameService rpsGameService;

    @MockBean
    private RpsStatisticsService rpsStatisticsService;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    public void getPlayRoundReturnDrawWhenPlayersShapesAreEquals() throws Exception {
        MultiValueMap<String, String> queryParams =  new LinkedMultiValueMap<>();
        RequestUtils.addNotNullParamToQueryMap(PLAYER1, "Rock", queryParams);
        RequestUtils.addNotNullParamToQueryMap(PLAYER2, "Rock", queryParams);
        when(rpsGameService.playRound("Rock", "Rock")).thenReturn(buildRpsResponse("Draw"));

        this.mockMvc.perform(
                get(RequestUtils.buildRequest(PLAY_ROUND_REQUEST_PATH_PARAMS, queryParams)))
                    .andExpect(status().is2xxSuccessful())
                    .andExpect(jsonPath(MESSAGE).value("Draw"))
                    .andReturn();
    }

    @Test
    public void getPlayRoundReturnWinnerWhenPlayersShapesAreDifferent() throws Exception {
        MultiValueMap<String, String> queryParams =  new LinkedMultiValueMap<>();
        RequestUtils.addNotNullParamToQueryMap(PLAYER1, "Rock", queryParams);
        RequestUtils.addNotNullParamToQueryMap(PLAYER2, "Scissors", queryParams);
        when(rpsGameService.playRound("Rock", "Scissors")).thenReturn(buildRpsResponse("Player 1 wins"));

        this.mockMvc.perform(
                get(RequestUtils.buildRequest(PLAY_ROUND_REQUEST_PATH_PARAMS, queryParams)))
                    .andExpect(status().is2xxSuccessful())
                    .andExpect(jsonPath(MESSAGE).value("Player 1 wins"))
                    .andReturn();
    }

    @Test
    public void getStatisticsReturnOKWhenStatisticsAreAvailable() throws Exception {

        when(rpsStatisticsService.getStatistics()).thenReturn(buildRpsStatisticsResponse());

        this.mockMvc.perform(
                get(GET_STATISTICS_REQUEST))
                    .andExpect(status().is2xxSuccessful())
                    .andExpect(content().json(STATS_JSON_RESPONSE))
                    .andReturn();
    }

    private RpsGameResponse buildRpsResponse(String message) {
        return RpsGameResponse.builder().message(message).build();
    }

    private RpsStatisticsResponse buildRpsStatisticsResponse() {
        return RpsStatisticsResponse.builder().totalRoundPlayed(4).totalPlayer1Wins(1).totalPlayer2Wins(2).totalDraws(1).build();
    }
}


