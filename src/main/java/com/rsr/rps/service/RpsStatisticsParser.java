package com.rsr.rps.service;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RpsStatisticsParser {
    private final ObjectMapper mapper = new ObjectMapper();

    public RpsStatisticsResponse parseStatistics(HttpResponse httpResponse) {
        try {
            return mapper.readValue(httpResponse.getEntity().getContent(), RpsStatisticsResponse.class);
        } catch (IOException ex) {
            log.error("Exception {} during parsing input message {}", ex.getMessage(), httpResponse);
        }
        return null;
    }
}