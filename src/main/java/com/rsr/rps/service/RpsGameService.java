package com.rsr.rps.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rsr.rps.model.PlayResult;
import com.rsr.rps.model.ShapeFactory;

/**
 * @author Rafa Sanchez
 */
@Service
public class RpsGameService {

    private final RpsStatisticsService rpsStatisticsService;

    @Autowired
    public RpsGameService(RpsStatisticsService rpsStatisticsService) {
        this.rpsStatisticsService = rpsStatisticsService;
    }

    public RpsGameResponse playRound(String player1Shape, String player2Shape) {
        PlayResult result = ShapeFactory.getShape(player1Shape).playAgainst(ShapeFactory.getShape(player2Shape));
        rpsStatisticsService.addPlayResultToStatistic(result.getMessage());
        return RpsGameResponse.builder().message(result.getMessage()).build();
    }
}
