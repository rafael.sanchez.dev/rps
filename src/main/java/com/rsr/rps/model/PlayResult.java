package com.rsr.rps.model;

/**
 * @author Rafa Sanchez
 */
public enum PlayResult {
    WIN("Player 1 wins"),
    LOOSE("Player 2 wins"),
    DRAW("Draw");

    private String message;

    PlayResult(String message) {
        this.message = message;
    }

    public final String getMessage() {
        return message;
    }
}
