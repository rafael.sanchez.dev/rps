package com.rsr.rps.model;

import lombok.Builder;

/**
 * @author Rafa Sanchez
 */
@Builder
public class Scissors implements Shape {

    @Override
    public PlayResult playAgainst(Shape shape) {
        if(shape instanceof Scissors) {
            return PlayResult.DRAW;
        } else if(shape instanceof Rock) {
            return PlayResult.LOOSE;
        }
        return PlayResult.WIN;
    }
}
