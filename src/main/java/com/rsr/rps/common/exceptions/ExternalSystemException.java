package com.rsr.rps.common.exceptions;

/**
 * @author Rafa Sanchez
 */
public class ExternalSystemException extends RuntimeException{

    public ExternalSystemException(String message) {
        super(message);
    }

}
