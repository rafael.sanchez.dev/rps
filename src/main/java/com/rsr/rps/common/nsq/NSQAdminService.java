package com.rsr.rps.common.nsq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.rsr.rps.common.config.RpsConfig;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Rafa Sanchez
 */

@Service
@Slf4j
public class NSQAdminService {
    private static final String HTTP_PROTOCOL_HEADER = "http://";
    private static final String CREATE_TOPIC_URI = "/topic/create?topic=";

    private RpsConfig config;

    @Autowired
    public NSQAdminService(RpsConfig config) {
        this.config = config;
    }

    public void createNsqTopic() {
        try {
            log.info("Creating Nsq statistic topic");
            WebClient postClient = getNSQCreateWebClient(config.getStatisticsNsqCreatePort());
            createTopic(postClient);
        } catch (Exception e) {
            log.error("Error {} in createNsqTopic", e.getMessage());
        }
    }

    private void createTopic(WebClient webClient) {
        String topic = config.getStatisticsTopic();
        if(topicAlreadyExist(topic)) {
            log.info("{} exists... skipping creation.", topic);
        } else {
            postTopic(webClient, CREATE_TOPIC_URI + topic);
            log.info("Topic {} created", topic);
        }
    }

    private boolean topicAlreadyExist(String topic) {
        return getListOfTopics(getNSQListWebClient()).getTopics().stream().anyMatch(topic::equals);
    }

    private NSQTopics getListOfTopics(WebClient webClient) {
        return webClient.get().uri("/topics").accept(MediaType.APPLICATION_JSON_UTF8).retrieve()
                        .bodyToMono(NSQTopics.class).block();
    }

    private void postTopic(WebClient webClient, String uri) {
        webClient.post().uri(uri).exchange().block();
        log.warn("Posting topic: {}", uri);
    }

    private WebClient getNSQListWebClient() {
        return WebClient.builder().baseUrl(getNsqUrl(config.getStatisticsLookupPort())).build();
    }

    private WebClient getNSQCreateWebClient(int nsqPort) {
        return WebClient.builder().baseUrl(getNsqUrl(nsqPort)).build();
    }

    private String getNsqUrl(int nsqPort) {
        String nsqHost = config.getStatisticsLookupHost();
        return HTTP_PROTOCOL_HEADER + nsqHost + ":" + nsqPort;
    }
}
