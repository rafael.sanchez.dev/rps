package com.rsr.rps.client;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.concurrent.TimeoutException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.github.brainlag.nsq.NSQProducer;
import com.github.brainlag.nsq.exceptions.NSQException;
import com.rsr.rps.common.config.RpsConfig;
import com.rsr.rps.model.PlayResult;

/**
 * @author Rafa Sanchez
 */
@RunWith(MockitoJUnitRunner.class)
public class StatisticsProducerTest {
    @Mock
    RpsConfig config;

    @Mock
    private NSQProducer nsqProducer;

    @InjectMocks
    private StatisticsProducer producer;

    @Before
    public void init() {
        when(config.getStatisticsTopic()).thenReturn("topic");
    }

    @Test
    public void postMessageReturnSuccessWhenNSQProducerIsOK() throws Exception {
        producer.postMessage(PlayResult.WIN.getMessage());

        verify(nsqProducer).produce(anyString(), any());
    }

    @Test(expected = RuntimeException.class)
    public void postMessageThrowsExceptionWhenNSQProducerReturnError() throws NSQException, TimeoutException {
        doThrow(NSQException.class).when(nsqProducer).produce(anyString(), any(byte[].class));

        producer.postMessage(null);
    }
}
