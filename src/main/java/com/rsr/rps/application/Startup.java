package com.rsr.rps.application;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rsr.rps.common.nsq.NSQAdminService;

/**
 * @author Rafa Sanchez
 */
@Component
public class Startup {

    @Autowired
    private NSQAdminService nsqAdminService;

    @PostConstruct
    void createNsqTopics() {
        nsqAdminService.createNsqTopic();
    }
}
