package com.rsr.rps.common.exceptions;

/**
 * @author Rafa Sanchez
 */
public class RpsException extends RuntimeException{

    public RpsException(String message) {
        super(message);
    }

}
