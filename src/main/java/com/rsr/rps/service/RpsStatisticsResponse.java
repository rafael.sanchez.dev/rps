package com.rsr.rps.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Rafa Sanchez
 */
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RpsStatisticsResponse {
    private int totalRoundPlayed;
    private int totalDraws;
    private int totalPlayer1Wins;
    private int totalPlayer2Wins;
}
