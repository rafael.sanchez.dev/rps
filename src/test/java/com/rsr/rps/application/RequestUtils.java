package com.rsr.rps.application;

import java.util.Arrays;

import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

public class RequestUtils {

    static String buildRequest(String pathParams, MultiValueMap<String, String> queryParams) {
        return pathParams.concat(UriComponentsBuilder.newInstance().queryParams(queryParams).build().toUriString());
    }

    static void addNotNullParamToQueryMap(String name, Object value, MultiValueMap<String, String> queryMap) {
        if (value != null) {
            if (value instanceof String[]) {
                Arrays.stream(((String[]) value)).forEach(s -> queryMap.add(name, s));
            } else {
                queryMap.add(name, String.valueOf(value));
            }
        }
    }
}