package com.rsr.rps.model;

import lombok.Builder;

/**
 * @author Rafa Sanchez
 */
@Builder
public class Rock implements Shape {

    @Override
    public PlayResult playAgainst(Shape shape) {
        if(shape instanceof Rock) {
            return PlayResult.DRAW;
        } else if(shape instanceof Paper) {
            return PlayResult.LOOSE;
        }
        return PlayResult.WIN;
    }
}
