package com.rsr.rps.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;

/**
 * @author Rafa Sanchez
 */
@Getter
@Component
public class RpsConfig {

    @Value("${statistics.nsq.producer.host}")
    private String statisticsProducerHost;

    @Value("${statistics.nsq.producer.port}")
    private int statisticsProducerPort;

    @Value("${statistics.nsq.topic}")
    private String statisticsTopic;

    @Value("${statistics.nsq.lookup.host}")
    private String statisticsLookupHost;

    @Value("${statistics.nsq.lookup.port}")
    private int statisticsLookupPort;

    @Value("${statistics.nsq.create.port}")
    private int statisticsNsqCreatePort;

    @Value("${statistics.http.host}")
    private String statisticsHttpHost;

    @Value("${statistics.http.port}")
    private int statisticsHttpPort;

    @Value("${statistics.endpoint}")
    private String statisticsEndpoint;
}
