package com.rsr.rps.client;

import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.brainlag.nsq.NSQProducer;
import com.github.brainlag.nsq.exceptions.NSQException;
import com.rsr.rps.common.config.RpsConfig;
import com.rsr.rps.common.exceptions.RpsException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Rafa Sanchez
 */
@Component
@Slf4j
public class StatisticsProducer {

    private final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private RpsConfig config;

    @Autowired
    private NSQProducer producer;

    public void postMessage(String body) {
        try {
            log.info("Adding to NSQ topic {}, message: {}", config.getStatisticsTopic(), body);
            producer.produce(config.getStatisticsTopic(), mapper.writeValueAsBytes(body));
        } catch (JsonProcessingException ex) {
            throw new RpsException("Error occurred while producing a new message");
        } catch (NSQException nsqEx) {
            throw new RpsException("Error occurred due to NSQ service");
        } catch (TimeoutException tex) {
            throw new RpsException("Timeout while producing a message for NSQ");
        }
    }

    @Bean
    public NSQProducer createProducer() {
        return new NSQProducer().addAddress(config.getStatisticsProducerHost(), config.getStatisticsProducerPort()).start();
    }
}
