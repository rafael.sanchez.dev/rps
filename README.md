The Rock - Paper - Scissors Game is built using 3 components:

    - Rps microservice - Has the business logic related to the game and it is the point of view for the client.
                         The round results are sent to a NSQ message queue in order to be stored by the statistics
                         microservice. The statistics are exposed by this microservice as well.

    - Statistics microservice - Store the information about the rounds and game result. Read the game results from NSQ
                                and process each message to store the information in the internal memory.

    - NSQ - built using a light nsq docker image provider by nsqio/nsq. To start the containers, run the setupRps.sh file


The client is in the rps-client project