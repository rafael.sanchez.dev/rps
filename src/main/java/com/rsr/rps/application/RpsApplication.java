package com.rsr.rps.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Rafa Sanchez
 */
@SpringBootApplication(scanBasePackages = {"com.rsr"})
public class RpsApplication {
    public static void main(String[] args) {
        SpringApplication.run(RpsApplication.class, args);
    }
}
